<?php
/*
 * Настройки находятся в ../config.php
 * Избегайте редактирования этого файла
 */
require __DIR__ . '/../libs/vendor/autoload.php';
require __DIR__ . '/../config.php';

$config = [
    'addContentLengthHeader' => false,
    'db' => [
        'driver' => 'mysql',
        'host' => DB_HOST,
        'port' => DB_PORT,
        'database' => DB_NAME,
        'username' => DB_USERNAME,
        'password' => DB_PASSWORD,
        'charset'   => DB_CHARSET,
        'collation' => 'utf8_unicode_ci',
        'prefix'    => '',
    ]
];
if (ENVIRONMENT === 'dev') {
    $config['displayErrorDetails'] = true;
}

$app = new Slim\App(["settings" => $config]);

// dependencies
require __DIR__ . '/dependencies.php';

// middleware
require __DIR__ . '/middleware.php';

// routes
require __DIR__ . '/routes.php';
$app->run();