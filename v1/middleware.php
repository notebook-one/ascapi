<?php
if (!defined('ENVIRONMENT')) {
    die('Direct access not permitted');
}

use Slim\Http\Request;
use Slim\Http\Response;

// Application middleware

$isBannedMiddleware = function (Request $request, Response $response, $next) {
    $ipAddress = Common::getRealUserIp($request);
    if ($this->BruteBlock->isBanned($ipAddress)) {
        throw new Exception('IP address is banned. Try again after ' . BAN_HOURS . ' hours', 401);
    }
    return $next($request, $response);
};

$app->add($isBannedMiddleware);

$mw = function (Request $request, Response $response, $next) {
    $apiKey = $request->getHeaderLine('X-ASC-AUTH');
    if ($apiKey !== API_KEY) {
        throw new Exception('Wrong API key', 401);
    }
    return $next($request, $response);
};


$app->add(function ($req, $res, $next) {
    $response = $next($req, $res);
    return $response
        ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
});
