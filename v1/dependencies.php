<?php
if (!defined('ENVIRONMENT')) {
    die('Direct access not permitted');
}

use Exceptions\ApiException;
use League\Fractal\Manager;
use League\Fractal\Serializer\ArraySerializer;
use Slim\Http\Request;

// DIC

$container = $app->getContainer();

$container['db'] = function ($c) {
    try {
        $pdo = new PDO('mysql:host=' . DB_HOST . ';port=' . DB_PORT . ';dbname=' . DB_NAME . ';charset=' . DB_CHARSET, DB_USERNAME, DB_PASSWORD);
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
        return $pdo;
    } catch (PDOException $e) {
        throw new Exception($e);
    }
};

$container['dbn'] = function ($container) {
    $capsule = new \Illuminate\Database\Capsule\Manager;
    $capsule->addConnection($container['settings']['db']);
    $capsule->setAsGlobal();
    $capsule->bootEloquent();

    return $capsule;
};


$container['fractal'] = function ($c) {
    $manager = new Manager();
    $manager->setSerializer(new DataArraySerializer());
    return $manager;
};

$container['OrderMapper'] = function ($c) {
    $dbService = $c->get('db');
    return new OrderMapper($dbService);
};

$container['ApiStatusCheckMapper'] = function ($c) {
    $dbService = $c->get('db');
    return new ApiStatusCheckMapper($dbService);
};

$container['BruteBlock'] = function ($c) {
    $dbService = $c->get('db');
    return new BruteBlock($dbService);
};

$container["errorHandler"] = function ($container) {
    return new ApiException;
};
