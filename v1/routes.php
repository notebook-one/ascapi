<?php
if (!defined('ENVIRONMENT')) {
    die('Direct access not permitted');
}

use Controllers\CustomerController;
use Controllers\GoodsController;
use Controllers\HookController;
use POCO\ApiStatusCheckEntity;
use Slim\Http\Request;
use Slim\Http\Response;

// Routes

$app->get('/orders/{id:[0-9]+}/customer-pass/{pass}', function (Request $request, Response $response, array $args) {
    $order = $this->OrderMapper->getOrder($args['id'], $args['pass']);
    if ($order) {
        $data = array('orderId' => $args['id'],
            'ip_address' => Common::getRealUserIp($request),
            'user_agent' => $request->getServerParam('HTTP_USER_AGENT'));
        $apiStatusCheck = new ApiStatusCheckEntity($data);
        $this->ApiStatusCheckMapper->save($apiStatusCheck);
        return $response->withJson($order);
    }
    $this->BruteBlock->addFailedLoginAttempt($request);
    throw new Exception("Not found", 404);
});

$app->get('/orders/customer-pass/{pass}', function (Request $request, Response $response, array $args) {
    $orders = $this->OrderMapper->getOrders($args);
    if ($orders) {
        return $response->withJson($orders);
    }
    $this->BruteBlock->addFailedLoginAttempt($request);
    throw new Exception("Not found", 404);
});

$app->group('/customers', function () use ($app) {
    $app->get('/', CustomerController::class . ':getCustomers');
    $app->get('/{id:[0-9]+}', CustomerController::class . ':getCustomer');
})->add($mw);

$app->group('/hooks/telephony', function () use ($app) {
    $app->post('/zadarma', HookController::class . ':saveZadarma');
    $app->get('/zadarma', HookController::class . ':ZadarmaEcho');
    $app->post('/rostelecom[/{event}/]', HookController::class . ':saveRostelecom');
    $app->post('/mango/events[/{event}]', HookController::class . ':saveMangotelecom');
    $app->post('/megafon', HookController::class . ':saveMegafon');
});
$app->group('/hooks/callback', function () use ($app) {
    $app->post('/', HookController::class . ':saveCallback');
});

$app->group('/hooks/opencart', function () use ($app) {
    $app->post('/orders/create', HookController::class . ':OrderCreate');
    $app->post('/orders/change', HookController::class . ':OrderChanged');
    $app->post('/customers/create', HookController::class . ':customerCreate');
})->add($mw);

$app->group('/goods', function () use ($app) {
    $app->get('/', GoodsController::class . ':getGoods');
    $app->get('/{id:[0-9]+}', GoodsController::class . ':getGood');
});


