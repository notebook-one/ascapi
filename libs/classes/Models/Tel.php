<?php
/**
 * Created by PhpStorm.
 * User: Ratz
 * Date: 29.03.2019
 * Time: 1:56
 */

namespace Models;


use Illuminate\Database\Eloquent\Model;

class Tel extends Model
{
    protected $table = 'tel';

    protected $fillable = [
        'phone',
        'phone_clean',
        'customer',
        'type',
        'note',
    ];

    public function customer()
    {
        return $this->belongsTo(Customer::class, 'customer');
    }
}