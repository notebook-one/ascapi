<?php
/**
 * Created by PhpStorm.
 * User: Ratz
 * Date: 29.03.2019
 * Time: 18:45
 */

namespace Models;


use Illuminate\Database\Eloquent\Model;

class Condition extends Model
{
    protected $table = 'items_state';

    protected $fillable = [
        'name',
    ];

    public function goods()
    {
        return $this->hasMany(Good::class, 'state');
    }
}