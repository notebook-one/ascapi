<?php
/**
 * Created by PhpStorm.
 * User: Ratz
 * Date: 29.03.2019
 * Time: 18:57
 */

namespace Models;


class Warranty
{

    public function getAll(){
        return [
            0 => '-нет-',
            7 => '7 дней',
            14 => '14 дней',
            31 => '1 месяц',
            62 => '2 месяца',
            93 => '3 месяца',
            124 => '4 месяца',
            155 => '5 месяцев',
            186 => '6 месяцев',
            217 => '7 месяцев',
            248 => '8 месяцев',
            279 => '9 месяцев',
            310 => '10 месяцев',
            341 => '11 месяцев',
            365 => '1 год',
            730 => '2 года',
            1095 => '3 года',
        ];
    }

    public function getById($key){
        $all = $this->getAll();
        return isset($all[$key]) ? $all[$key] : null;
    }
}