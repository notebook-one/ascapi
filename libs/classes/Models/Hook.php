<?php
/**
 * Created by PhpStorm.
 * User: Ratz
 * Date: 29.03.2019
 * Time: 14:54
 */

namespace Models;


use Illuminate\Database\Eloquent\Model;

class Hook extends Model
{
    protected $table = 'hooks';
    // protected $dates = ['created_at', 'updated_at'];
    protected $fillable = [
        'hook_id',
        'status',
        'created_at',
        'updated_at',
        'event',
        'p0',
        'prm',
    ];

    /* RT Only
    public function getDateFormat()
    {
        return 'Y-m-d H:i:s.u';
    }
    */
}