<?php
/**
 * Created by PhpStorm.
 * User: Ratz
 * Date: 24.03.2019
 * Time: 17:24
 */

namespace Models;


use Illuminate\Database\Eloquent\Model;

class Good extends Model
{
    protected $table = 'store_items';

    public function category(){
        return $this->belongsTo(Category::class, 'category');
    }

    public function condition(){
        return $this->belongsTo(Condition::class, 'state');
    }

    public function attributes()
    {
        return $this->hasMany(FieldValue::class, 'item_id');
    }

    public function images()
    {
        return $this->hasMany(Image::class, 'item_id');
    }
}