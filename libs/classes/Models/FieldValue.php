<?php
namespace Models;


    use Illuminate\Database\Eloquent\Model;

class FieldValue extends Model
{
    protected $table = 'field_values';

    public function field(){
        return $this->belongsTo(Field::class, 'field_id');
    }
}