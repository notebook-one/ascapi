<?php
/**
 * Created by PhpStorm.
 * User: Ratz
 * Date: 24.03.2019
 * Time: 17:24
 */

namespace Models;


use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    protected $table = 'images';
}