<?php
/**
 * Created by PhpStorm.
 * User: Ratz
 * Date: 29.03.2019
 * Time: 17:26
 */

namespace Models;


use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'store_cats';

    protected $fillable = [
        'name',
        'parent',
        'position',
        'enable',
        'store_id',
    ];

    protected $casts = [
        'enable' => 'boolean',
    ];

    public function goods()
    {
        return $this->hasMany(Good::class, 'category');
    }

    public function parentCategory()
    {
        return $this->belongsTo(self::class, 'parent');
    }

    public function childrenCategories()
    {
        return $this->hasMany(self::class, 'parent');
    }

    public function getParentsNames() {
        if($this->parentCategory) {
            return $this->parentCategory->getParentsNames(). " | " . $this->name;
        } else {
            return $this->name;
        }
    }

}