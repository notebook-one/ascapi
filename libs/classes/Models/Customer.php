<?php
/**
 * Created by PhpStorm.
 * User: Ratz
 * Date: 28.03.2019
 * Time: 18:59
 */
namespace Models;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $table = 'clients';

    protected $fillable = [
        'type',
        'surname',
        'name',
        'patronymic',
        'ur_name',
        'email',
    ];

    public function phones()
    {
        return $this->hasMany(Tel::class, 'customer');
    }
}