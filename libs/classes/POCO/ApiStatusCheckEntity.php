<?php

namespace POCO;

class ApiStatusCheckEntity
{
    public $Id;
    public $Created;
    public $IpAddress;
    public $OrderId;
    public $UserAgent;

    public function __construct(array $data)
    {
        if (isset($data['id'])) {
            $this->Id = $data['id'];
        }
        $this->OrderId = $data['orderId'];
        $this->IpAddress = $data['ip_address'];
        $this->UserAgent = $data['user_agent'];
    }
}