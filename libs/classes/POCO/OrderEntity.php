<?php

namespace POCO;

class OrderEntity
{
    public $Id;
    public $Type;
    public $Trademark;
    public $Model;
    public $SerialNumber;
    public $DiagnosticResult;
    public $InDate;
    public $IssueDate;
    public $Fault;
    public $Customer;
    public $StatusName;
    public $StatusId;

    public $OfficeTel;
    public $Manager;

    public $DiagSumm;
    public $Summ;

    public function __construct(array $data)
    {
        if (isset($data['id'])) {
            $this->Id = $data['id'];
        }
        $this->setStatusId($data['state']);
        $this->setCustomer($data);
        $this->setDiagSumm($data['repair_cost']);
        $this->setSumm($data['real_repair_cost']);
        $this->setManager($data);
        $this->OfficeTel = $data['officeTel'];
        $this->Type = $data['device'];
        $this->Trademark = $data['trademark'];
        $this->Model = $data['model'];
        $this->SerialNumber = $data['serial_number'];
        $this->DiagnosticResult = $data['diagnostic_result'];
        $this->InDate = $data['in_date'];
        $this->IssueDate = $data['out_date'];
        $this->Fault = $data['fault'];
    }

    protected function setDiagSumm($value)
    {
        $this->DiagSumm = number_format((float)$value, 2, '.', '');
    }

    protected function setManager($data)
    {
        // $this->Manager = $data['managerLastName'] . ' ' .$data['managerFirstName'] . ' ' . $data['managerMiddleName'];
        $this->Manager = $data['managerFirstName'] . ' ' . $data['managerMiddleName'];
    }

    protected function setSumm($value)
    {
        $this->Summ = number_format((float)$value, 2, '.', '');
    }

    protected function setStatusId($statusId)
    {
        $this->StatusId = (int)$statusId;
    }

    protected function setCustomer($data)
    {
        $this->Customer = $data['type'] === 1 ? $data['ur_name'] : $data['lastName'] . ' ' . $data['firstName'] . ' ' . $data['middleName'];
    }
}