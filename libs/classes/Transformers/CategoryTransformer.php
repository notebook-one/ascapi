<?php
/**
 * Created by PhpStorm.
 * User: Ratz
 * Date: 29.03.2019
 * Time: 18:00
 */

namespace Transformers;


use League\Fractal\TransformerAbstract;
use Models\Category;

class CategoryTransformer extends TransformerAbstract
{

    public function transform(Category $c)
    {
        return [
            "name" => $c->name,
            "parent" => $c->parent,
            "position" => $c->position,
            "enable" => $c->enable,
            "store_id" => $c->store_id,
        ];
    }
}