<?php
/**
 * Created by PhpStorm.
 * User: Ratz
 * Date: 29.03.2019
 * Time: 2:11
 */

namespace Transformers;


use League\Fractal\TransformerAbstract;
use Models\Tel;

class TelTransformer extends TransformerAbstract
{

    public function transform(Tel $c)
    {
        return [
            "phone" => (string)$c->phone,
            "phone_clean" => (string)$c->phone_clean,
            "customer" => (int)$c->customer,
            "type" => (int)$c->type,
            "note" => (string)$c->note,
        ];
    }
}