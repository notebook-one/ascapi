<?php
/**
 * Created by PhpStorm.
 * User: Ratz
 * Date: 29.03.2019
 * Time: 18:05
 */

namespace Transformers;


use League\Fractal\TransformerAbstract;
use Models\Good;
use Models\Warranty;

class GoodTransformer extends TransformerAbstract
{
    protected $warranty;

    public function __construct()
    {
        $this->warranty = new Warranty();
    }

    public function transform(Good $c)
    {
        $output = [
            "Id" => sprintf("%06d", $c->id),
            "Articul" => sprintf("%06d", $c->articul),
            "Name" => $c->name,
            "Category" => $c->getRelationValue('category')->name,
            "CategoryTree" => $c->getRelationValue('category')->getParentsNames(),
            "Condition" => $c->condition()->pluck('name'),
            "Description" => $c->description,
            "ShopTitle" => $c->shop_title,
            "ShopDescription" => $c->shop_description,
            "Quantity" => $c->count - $c->reserved,
            "Price" => $c->price2,
            "PriceOpt" => $c->price3,
            "PriceOpt2" => $c->price4,
            "PriceOpt3" => $c->price5,
            "Partnumber" => $c->PN,
            "AscBarcode" => (string)$c->int_barcode,
            "Warranty" => $this->warranty->getById($c->warranty),
            "ATTRIBUTES" => $this->getAttributes($c)
        ];
        foreach ($c->attributes as $attribute) {
            if (empty($attribute->value) || !count($attribute->field)) continue;

            if ($attribute->field->required == 1) {
                $output[$attribute->field->name] = $attribute->value;
            } else {
                $output['ATTRIBUTE'][] = [
                    'Name' => $attribute->field->name,
                    'Value' => $attribute->value
                ];
            }
        }
        if ($c->relationLoaded('images')) {
            $images = array();
            foreach ($c->images as $image) {
                $images[] = array(
                    'img' => base64_encode($image->img),
                    'preview' => base64_encode($image->preview),
                    'added' => $image->added);
            }
            $output['Images'] = $images;
        }
        return $output;
    }

    private function getAttributes(Good $c)
    {
        $attr = array();
        $attr['ATTRIBUTE'][] = ['Name' => 'Гарантия',
            'Value' => $this->warranty->getById($c->warranty)];
        $attr['ATTRIBUTE'][] = [
            'Name' => 'Состояние',
            'Value' => $c->condition()->pluck('name')
        ];
        $attr['ATTRIBUTE'][] = [
            'Name' => 'Партномер',
            'Value' => $c->PN
        ];
        return $attr;
    }
}