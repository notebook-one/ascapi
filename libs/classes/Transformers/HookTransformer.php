<?php
/**
 * Created by PhpStorm.
 * User: Ratz
 * Date: 29.03.2019
 * Time: 17:17
 */

namespace Transformers;


use League\Fractal\TransformerAbstract;
use Models\Hook;

class HookTransformer extends TransformerAbstract
{
    public function transform(Hook $hook)
    {
        return [
            "hook_id" => (int)$hook->hook_id,
            "status" => (int)$hook->status,
            "created_at" => $hook->created_at,
            "updated_at" => $hook->updated_at,
            "event" => $hook->event,
            "p0" => $hook->p0,
            "prm" => $hook->prm
        ];
    }
}