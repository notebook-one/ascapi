<?php
/**
 * Created by PhpStorm.
 * User: Ratz
 * Date: 28.03.2019
 * Time: 21:33
 */

namespace Transformers;


use League\Fractal\TransformerAbstract;
use Models\Customer;

class CustomerTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [
        'phones',
    ];

    public function transform(Customer $c)
    {
        return [
            "id" => (int)$c->id,
            "type" => $c->type,
            "last_name" => $c->surname,
            "first_name" => $c->name,
            "patronymic" => $c->patronymic,
            "ur_name" => $c->ur_name,
            "email" => $c->email,
            "createdAt" => $c->created,
        ];
    }

    public function includePhones(Customer $c)
    {
        return $this->collection($c->phones, new TelTransformer, false);
    }

}