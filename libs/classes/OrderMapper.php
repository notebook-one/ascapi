<?php

use POCO\OrderEntity;

class OrderMapper extends Mapper
{
    public function getOrder($id, $pass)
    {
        $sql = 'SELECT w.id, w.state, d.name AS device, ma.name AS trademark, mo.name AS model,
                  w.serial_number, w.diagnostic_result, w.in_date, w.out_date,
                  w.real_repair_cost, w.repair_cost, w.fault, w.complect, c.type,
                  c.surname AS lastName, c.name AS firstName, c.patronymic as middleName, c.ur_name,
                  usr.name AS managerFirstName, usr.surname AS managerLastName, usr.patronymic AS managerMiddleName,
                  off.phone AS officeTel
            from workshop w
            join devices d on (d.id = w.type)
            join device_makers ma on (ma.id = w.maker)
            join device_models mo on (mo.id = w.model)
            join users usr on (usr.id = w.current_manager)
            join offices off on (off.id = usr.office)
            join clients c on (c.id = w.client)
            where w.id = :id AND c.web_password = :pass';
        $stmt = $this->db->prepare($sql);
        $result = $stmt->execute(['id' => $id, 'pass' => $pass]);
        if ($result) {
            $data = $stmt->fetch();
            if (!$data) {
                return null;
            }
            $order = new OrderEntity($data);
            $mapper = new StatusMapper($this->db);
            $order->StatusName = $mapper->getStatusName($order->StatusId);
            return $order;
        }
    }

    public function getOrders($args)
    {
        $sql = 'SELECT w.id, w.state, d.name AS device, ma.name AS trademark, mo.name AS model,
                  w.serial_number, w.diagnostic_result, w.in_date, w.out_date,
                  w.real_repair_cost, w.repair_cost, w.fault, w.complect, c.type,
                  c.surname AS lastName, c.name AS firstName, c.patronymic as middleName, c.ur_name
            from workshop w
            join devices d on (d.id = w.type)
            join device_makers ma on (ma.id = w.maker)
            join device_models mo on (mo.id = w.model)
            join clients c on (c.id = w.client)
            where c.web_password = :pass';
        $stmt = $this->db->prepare($sql);
        $result = $stmt->execute(['pass' => $args['pass']]);
        if ($result) {
            $r = array();
            while ($data = $stmt->fetch()) {
                $order = new OrderEntity($data);
                $mapper = new StatusMapper($this->db);
                $order->StatusName = $mapper->getStatusName($order->StatusId);
                $r[] = $order;
            }
            return $r;
        }
    }


}