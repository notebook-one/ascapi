<?php

use POCO\ApiStatusCheckEntity;

class ApiStatusCheckMapper extends Mapper
{

    public function save(ApiStatusCheckEntity $checkEntity) {
        $sql = 'INSERT INTO api_status_checks
            (repair, ip_address, user_agent, created) VALUES
            (:repair, INET_ATON(:ip_address), :user_agent, NOW())';
        $stmt = $this->db->prepare($sql);
        $result = $stmt->execute([
            "repair" => $checkEntity->OrderId,
            "ip_address" => $checkEntity->IpAddress,
            "user_agent" => $checkEntity->UserAgent
        ]);
        if(!$result) {
            throw new Exception("could not save record");
        }
    }
}