<?php

use Slim\Http\Request;

class Common
{
    public static function getRealUserIp(Request $request)
    {
        switch (true) {
            case (!empty($request->getServerParam('HTTP_X_REAL_IP'))) :
                return $request->getServerParam('HTTP_X_REAL_IP');
            case (!empty($request->getServerParam('HTTP_CLIENT_IP'))) :
                return $request->getServerParam('HTTP_CLIENT_IP');
            case (!empty($request->getServerParam('HTTP_X_FORWARDED_FOR'))) : {
                if (strpos($request->getServerParam('HTTP_X_FORWARDED_FOR'), ',') > 0) {
                    $addr = explode(",", $request->getServerParam('HTTP_X_FORWARDED_FOR'));
                    return trim($addr[0]);
                } else {
                    return $request->getServerParam('HTTP_X_FORWARDED_FOR');
                }
            }
            default :
                return $request->getServerParam('REMOTE_ADDR');
        }
    }
}