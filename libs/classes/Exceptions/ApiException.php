<?php
/**
 * Created by PhpStorm.
 * User: Ratz
 * Date: 23.12.2018
 * Time: 22:31
 */

namespace Exceptions;

use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Slim\Handlers\Error;

class ApiException extends Error
{
    public function __invoke(Request $request, Response $response, \Exception $exception)
    {
        $status = $exception->getCode() ?: 500;
        $data = [
            "status" => "error",
            "message" => $exception->getMessage(),
        ];
        $body = json_encode($data, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT);
        return $response
            ->withStatus($status)
            ->withHeader("Content-type", "application/json")
            ->write($body);
    }
}