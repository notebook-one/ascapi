<?php

class StatusMapper extends Mapper
{
    // protected $Statuses;

    protected function getStatuses()
    {
        $sql = 'SELECT statuses
            from config c
            where c.id = 1';
        $stmt = $this->db->prepare($sql);
        $result = $stmt->execute();
        if ($result) {
            return json_decode($stmt->fetch()['statuses']);
        }
    }

    public function getStatusName($statusId)
    {
        foreach ($this->getStatuses() as $status) {
            if ($status->Id === $statusId) {
                return $status->Name;
            }
        }
    }

    public function getStatus($statusId){

    }
}