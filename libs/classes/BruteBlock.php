<?php

class BruteBlock
{
    protected $db;

    public function __construct($db)
    {
        $this->db = $db;
    }

    public function isBanned($ip)
    {
        $sql = 'SELECT COUNT(*) FROM api_failed_logins WHERE ip_address = INET_ATON(:ip) AND attempted_at > DATE_SUB(NOW(), INTERVAL '.BAN_HOURS.' HOUR);';
        $stmt = $this->db->prepare($sql);
        try {
            $stmt->execute(['ip' => $ip]);
        } catch (PDOException $e) {
            throw new Exception($e);
        }
        $rows = $stmt->fetchColumn();
        return $rows >= MAX_LOGIN_ATTEMPTS;
    }

    public function addFailedLoginAttempt($request){
        $ipAddress = Common::getRealUserIp($request);
        if(AUTO_CLEAR_FAILED_LOGIN){
            $this->clearLoginAttempts();
        }
        $sql = 'INSERT INTO api_failed_logins SET ip_address = INET_ATON(:ip), attempted_at = NOW();';
        $stmt = $this->db->prepare($sql);
        $stmt->execute(['ip' => $ipAddress]);
    }

    private function clearLoginAttempts(){
        $sql = 'DELETE from api_failed_logins WHERE attempted_at < DATE_SUB(NOW(), INTERVAL 12 HOUR);';
        $stmt = $this->db->prepare($sql);
        $stmt->execute();
    }
}