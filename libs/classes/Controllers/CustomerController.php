<?php
/**
 * Created by PhpStorm.
 * User: Ratz
 * Date: 25.03.2019
 * Time: 15:49
 */

namespace Controllers;

use Interop\Container\ContainerInterface;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;
use Models\Customer;
use Slim\Http\Request;
use Slim\Http\Response;
use Transformers\CustomerTransformer;

class CustomerController
{
    protected $db;
    protected $fractal;

    public function __construct(ContainerInterface $container)
    {
        $this->fractal = $container->get('fractal');
        $this->db = $container->get('dbn');
    }

    public function getCustomer(Request $request, Response $response, array $args)
    {
        $customer = Customer::query()->where('id', $args['id'])->firstOrFail();
        $data = $this->fractal->createData(new Item($customer, new CustomerTransformer()))->toArray();
        return $response->withJson(['customer' => $data]);
    }


    public function getCustomers(Request $request, Response $response)
    {
        $filter = $request->getParam('filter', null);
        $page = $request->getParam('page', 1);
        $limit = $request->getParam('limit', 10);

        $offset = (--$page) * $limit;
        $builder = Customer::query()
            ->where('state', 1)
            ->with('phones')
            ->skip($offset)
            ->take($limit);

        if ($filter != null) {
            $builder->where('email', $filter)
                ->orWhere('surname', $filter)
                ->orWhere('name', $filter)
                ->orWhere('ur_name', $filter);
        }
        $customersCount = $builder->count();
        $customers = $builder->get();
        $data = $this->fractal->createData(new Collection($customers,
            new CustomerTransformer()))->toArray();
        return $response->withJson(['customers' => $data['data'], 'customersCount' => $customersCount]);
    }

    public function create(Request $request, Response $response)
    {

    }
}