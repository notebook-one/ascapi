<?php

namespace Controllers;

use Common;
use Exception;
use League\Fractal\Resource\Item;
use Models\Hook;
use Slim\Http\Request;
use Slim\Http\Response;
use Hooks;
use Interop\Container\ContainerInterface;
use Transformers\HookTransformer;

class HookController
{
    protected $remoteIp;
    protected $db;
    protected $fractal;

    public function __construct(ContainerInterface $container)
    {
        $this->fractal = $container->get('fractal');
        $this->db = $container->get('dbn');
    }

    public function saveRostelecom(Request $request)
    {
        $this->remoteIp = Common::getRealUserIp($request);
        $entity = $this->defaultEntity($request);


        $json = $request->getBody()->getContents();
        $signature = $request->getHeaderLine('X-Client-Sign');
        $signatureTest = hash("sha256", RT_API_ID . $json . RT_API_KEY);

        if ($signature !== $signatureTest) {
            throw new Exception('Signature test failed', 401);
        }
        $type = strtoupper($request->getParsedBodyParam('type'));
        $entity['created_at'] = $request->getParsedBodyParam('timestamp', date("Y-m-d H:i:s"));
        $entity['hook_id'] = Hooks::Rostelecom;
        // state like  new, connected, disconnected
        $entity['event'] = strtoupper($request->getParsedBodyParam('state')) . '_' . $type;
        if ($type === 'OUTBOUND') {
            $entity['p0'] = $request->getParsedBodyParam('from_pin', null);
        }
        if ($type === 'INCOMING') {
            $entity['p0'] = $request->getParsedBodyParam('request_pin', null);
        }
        Hook::create($entity);
    }

    public function saveMangotelecom(Request $request)
    {
        $this->remoteIp = Common::getRealUserIp($request);
        if (!in_array($this->remoteIp, ['81.88.80.132', '81.88.80.133', '81.88.82.36'])) {
            throw new Exception('Remote IP address wrong', 401);
        }
        $apiKey = $request->getParsedBodyParam('vpbx_api_key', null);
        $sign = $request->getParsedBodyParam('sign', null);
        if ($apiKey != MANGO_API_KEY) {
            throw new Exception('Unauthorized access', 401);
        }
        // TODO: sign check
        $entity = $this->defaultEntity($request);
        $json = json_decode($request->getParsedBodyParam('json', ""));
        if(!empty($json->to->extension)){
            $entity['p0'] = $json->to->extension;
        }
        if(!empty($json->call_state)){
            $entity['event'] = strtoupper($json->call_state);
        }

        $entity['prm'] = json_encode($json);
        $entity['hook_id'] = Hooks::Mangotelecom;
        Hook::create($entity);
    }

    public function saveMegafon(Request $request)
    {
        $token = $request->getParsedBodyParam('crm_token', null);
        if($token != API_KEY){
            throw new Exception('Unauthorized access', 401);
        }

        $command = $request->getParsedBodyParam('cmd', null);
        if($command == "event"){
            $entity = $this->defaultEntity($request);
            $entity['event'] = $request->getParsedBodyParam('type', null);
            $entity['p0'] = $request->getParsedBodyParam('ext', null);
            $entity['hook_id'] = Hooks::Megafon;
            Hook::create($entity);
        }
    }

    public function saveZadarma(Request $request)
    {
        $this->remoteIp = Common::getRealUserIp($request);
        $entity = $this->defaultEntity($request);
        if ($this->remoteIp !== ZADARMA_IP) {
            throw new Exception('Remote IP address wrong', 401);
        }
        $signature = $request->getHeaderLine('Signature');
        $callerId = $request->getParsedBodyParam('caller_id', $default = null);
        $calledDid = $request->getParsedBodyParam('called_did', $default = null);
        $callStart = $request->getParsedBodyParam('call_start', $default = null);
        $signatureTest = base64_encode(hash_hmac('sha1', $callerId . $calledDid . $callStart, ZADARMA_API_SECRET));

        if ($signature !== $signatureTest) {
            throw new Exception('Signature test failed', 401);
        }
        $entity['created_at'] = $request->getParsedBodyParam('call_start', date("Y-m-d H:i:s"));
        $entity['hook_id'] = Hooks::Zadarma;
        $entity['event'] = $request->getParsedBodyParam('event');
        $entity['p0'] = $request->getParsedBodyParam('internal', null);
        Hook::create($entity);
    }

    public function ZadarmaEcho(Request $request)
    {
        $params = $request->getQueryParams();
        $zdEcho = $params["zd_echo"];
        if (isset($zdEcho)) {
            exit($zdEcho);
        }
    }

    public function saveCallback(Request $request, Response $response)
    {
        $entity = $this->defaultEntity($request);
        $entity['hook_id'] = Hooks::Callback;
        $entity['event'] = strtoupper('CALLBACK');
        $hook = Hook::create($entity);
        return $this->hookResponse($response, $hook);
    }

    public function customerCreate(Request $request, Response $response)
    {
        $entity = $this->defaultEntity($request);
        $entity['hook_id'] = Hooks::OpenCart;
        $entity['event'] = strtoupper('NEW_CUSTOMER');
        $hook = Hook::create($entity);
        return $this->hookResponse($response, $hook);
    }

    public function OrderCreate(Request $request, Response $response)
    {
        $entity = $this->defaultEntity($request);
        $entity['hook_id'] = Hooks::OpenCart;
        $entity['event'] = strtoupper('NEW_ORDER');
        $hook = Hook::create($entity);
        return $this->hookResponse($response, $hook);
    }

    public function OrderChanged(Request $request, Response $response)
    {
        $entity = $this->defaultEntity($request);
        $entity['hook_id'] = Hooks::OpenCart;
        $entity['event'] = strtoupper('ORDER_CHANGED');
        $hook = Hook::create($entity);
        return $this->hookResponse($response, $hook);
    }

    /**
     * @param Request $request
     * @return array
     */
    private function defaultEntity(Request $request)
    {
        $entity = [
            'status' => 1,
            'prm' => json_encode($request->getParsedBody()),
            'updated_at' => null
        ];
        return $entity;
    }

    /**
     * @param Response $response
     * @param $hook
     * @return Response
     */
    private function hookResponse(Response $response, $hook)
    {
        $data = $this->fractal->createData(new Item($hook, new HookTransformer()))->toArray();
        return $response->withJson(['hook' => $data]);
    }
}