<?php
/**
 * Created by PhpStorm.
 * User: Ratz
 * Date: 29.03.2019
 * Time: 17:23
 */

namespace Controllers;


use ArrayToXML;
use Interop\Container\ContainerInterface;
use League\Fractal\Resource\Item;
use Models\Good;
use Slim\Http\Request;
use Slim\Http\Response;
use League\Fractal\Resource\Collection;
use Transformers\GoodTransformer;

class GoodsController
{
    protected $db;
    protected $fractal;

    public function __construct(ContainerInterface $container)
    {
        $this->fractal = $container->get('fractal');
        $this->db = $container->get('dbn');
    }

    public function getGoods(Request $request, Response $response, array $args){

        $format = $request->getParam('format', 'json');
        $page = $request->getParam('page', 1);
        $limit = $request->getParam('limit', 10);

        $offset = (--$page) * $limit;
        $builder = Good::query()
            ->with('category', 'condition')
            ->skip($offset)
            ->take($limit);
        $this->filterGoods($request, $builder);
        $count = $builder->count();
        $goods = $builder->get();
        $data = $this->fractal->createData(new Collection($goods,
            new GoodTransformer(), 'Product'))->toArray();

        if($format === strtolower('xml')){
            return $this->asXML($response, $data);
        }
        else{
            return $response->withJson(['goods' => $data['Product'], 'goodsCount' => $count]);
        }
    }

    public function getGood(Request $request, Response $response, array $args){
        $builder = Good::query()
            ->with('category', 'condition', 'images');
        $good = $builder->findOrFail($args['id']);
        $data = $this->fractal->createData(new Item($good,
            new GoodTransformer()))->toArray();
        return $response->withJson(['good' => $data['data']]);
    }

    /**
     * @param Response $response
     * @param $data
     * @param string $startElement
     * @return Response
     */
    private function asXML(Response $response, $data, $startElement = 'Products')
    {
        $result = new ArrayToXML();
        $newResponse = $response
            ->withHeader('Content-Type', 'text/xml');
        $newResponse->getBody()->write($result->buildXML($data, $startElement));

        return $newResponse;
    }

    /**
     * @param Request $request
     * @param $builder
     */
    private function filterGoods(Request $request, $builder)
    {
        $filter = $request->getParam('filter', array());

        if (isset($filter['name'])) {
            $builder->where('name', 'like', '%' . $filter['name'] . '%');
        }
        if (isset($filter['description'])) {
            $builder->where('description', 'like', '%' . $filter['description'] . '%');
        }
        if (isset($filter['pn'])) {
            $builder->where('PN', 'like', '%' . $filter['pn'] . '%');
        }
        if(isset($filter['shop_enable'])){
            $shopEnable = $filter['shop_enable'] === 'true'? true: false;
            $builder->where('shop_enable', '=', $shopEnable);
        }
        if(isset($filter['available_only'])){
            $availableOnly = $filter['available_only'] === 'true'? true: false;
            if($availableOnly){
                $builder->whereRaw('count-reserved-dealer_lock > 0');
            }

        }
        if(isset($filter['store'])){
            $builder->where('store', '=', $filter['store']);
        }
    }
}