<?php

/*
 * Параметры подключения к БД
 */
define('DB_HOST', 'localhost');
define('DB_PORT', '3306');
define('DB_NAME', 'ascnbdb');
define('DB_USERNAME', 'admin');
define('DB_PASSWORD', 'password');
define('DB_CHARSET', 'utf8');

define('API_KEY', '123456');
/*
 * Параметры блокировки по IP
 */
define('MAX_LOGIN_ATTEMPTS', 5);
define('BAN_HOURS', 12);
define('AUTO_CLEAR_FAILED_LOGIN', true);

/*
 * Параметры облачной телефонии от Zadarma
 */
define('ZADARMA_IP', '185.45.152.42');
define('ZADARMA_API_SECRET', 'wkU7GPvpqvNUrpzS'); // получить ключ можно тут: https://my.zadarma.com/api/

/*
 * Параметры облачной телефонии от Rostelecom
 */
define('RT_API_ID', 'wkU7GPvpqvNUrpzS'); // уникальный код идентификации
define('RT_API_KEY', 'wkU7GPvpqvNUrpzS'); // уникальный ключ для подписи

/*
 * Параметры облачной телефонии от Mangotelecom
 */
define('MANGO_API_KEY', 'wkU7GPvpqvNUrpzS');
define('MANGO_SIGN', 'wkU7GPvpqvNUrpzS');

/*
 * dev | prod
 */
define('ENVIRONMENT', 'dev');

